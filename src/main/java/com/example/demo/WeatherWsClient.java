package com.example.demo;

import com.example.demo.weather.ArrayOfString;
import com.example.demo.weather.WeatherWebService;
import com.example.demo.weather.WeatherWebServiceSoap;

/**
 * 天气查询
 */
public class WeatherWsClient {

    public static void main(String[] args) {
        //也可以使用new WeatherWebService(url)此方法可重新设置请求的地址 URL url=new URL("http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl")
        WeatherWebService factory = new WeatherWebService();
        WeatherWebServiceSoap weatherWebServiceSoap = factory.getWeatherWebServiceSoap(); //WeatherWebServiceSoap为调用的实现类
        ArrayOfString strArray = null;
        strArray = weatherWebServiceSoap.getWeatherbyCityName("西安");
        System.out.println(strArray.getString());
    }

}
