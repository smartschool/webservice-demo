package com.example.demo;

import com.example.demo.ipaddress.ArrayOfString;
import com.example.demo.ipaddress.IpAddressSearchWebService;
import com.example.demo.ipaddress.IpAddressSearchWebServiceSoap;

public class IpAddressWsClient {

    public static void main(String[] args) {
        //也可以使用new WeatherWebService(url)此方法可重新设置请求的地址 URL url=new URL("http://www.webxml.com.cn/WebServices/WeatherWebService.asmx?wsdl")
        IpAddressSearchWebService factory = new IpAddressSearchWebService();
        IpAddressSearchWebServiceSoap ipAddressSearchWebServiceSoap = factory.getIpAddressSearchWebServiceSoap(); //WeatherWebServiceSoap为调用的实现类
        ArrayOfString strArray = ipAddressSearchWebServiceSoap.getGeoIPContext();
        System.out.println(strArray.getString());
    }

}
