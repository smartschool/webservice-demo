package com.example.demo;

import com.example.demo.ChinaTVprogram.ChinaTVprogramWebService;
import com.example.demo.ChinaTVprogram.ChinaTVprogramWebServiceSoap;
import com.example.demo.ChinaTVprogram.GetAreaDataSetResponse;

/**
 * 中国电视节目预告 Web 服务</strong>，数据准确可靠，提供全国近800个电视台一个星期节目预告数据。
 * <a href="http://www.webxml.com.cn/" target="_blank">WebXml.com.cn</a>
 * 和/或其各供应商不为本页面提供信息的错误、残缺、延迟或因依靠此信息所采取的任何行动负责。
 */
public class ChinaTVprogramWsClient {

    public static void main(String[] args) {
        ChinaTVprogramWebService service = new ChinaTVprogramWebService();
        ChinaTVprogramWebServiceSoap soap = service.getChinaTVprogramWebServiceSoap();
        GetAreaDataSetResponse.GetAreaDataSetResult areaDataSetResult = soap.getAreaDataSet();
        System.out.println(areaDataSetResult.getAny());

        System.out.println(soap.getAreaString().getString());

        System.out.println(soap.getTVchannelString(51).getString());

        com.example.demo.ChinaTVprogram.GetTVchannelDataSetResponse.GetTVchannelDataSetResult tVchannelDataSetResult = soap.getTVchannelDataSet(194);
        System.out.println(tVchannelDataSetResult.getAny());

        System.out.println(soap.getTVstationString(101).getString());
    }

}
