package com.example.demo;

import com.example.demo.QQOnline.QqOnlineWebService;
import com.example.demo.QQOnline.QqOnlineWebServiceSoap;

/**
 * 腾讯QQ在线状态 WEB 服务</strong>。
 * <a href="http://www.webxml.com.cn/" target="_blank">WebXml.com.cn</a>
 * 和/或其各供应商不为本页面提供信息的错误、残缺、延迟或因依靠此信息所采取的任何行动负责。</br>
 */
public class QQOnlineWsClient {

    public static void main(String[] args) {
        QqOnlineWebService service = new QqOnlineWebService();
        QqOnlineWebServiceSoap soap = service.getQqOnlineWebServiceSoap();
        System.out.println(soap.qqCheckOnline("100000"));
    }
}
