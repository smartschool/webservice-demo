package com.example.demo;

import com.example.demo.DomesticAirline.DomesticAirline;
import com.example.demo.DomesticAirline.DomesticAirlineSoap;

/**
 * 国内飞机航班时刻表 WEB 服务</strong>提供：通过出发城市和到达城市查询飞机航班、出发和到达时间、飞行周期、航空公司、机型等信息。
 * 国内飞机航班时刻表 WEB 服务提供的飞机航班时刻表数据仅供参考，如有异议请以当地飞机场提供的讯息为准。
 */
public class DomesticAirlineWsClient {

    public static void main(String[] args) {
        DomesticAirline service = new DomesticAirline();
        DomesticAirlineSoap soap = service.getDomesticAirlineSoap();

        System.out.println(soap.getDomesticCity().getAny());

        System.out.println(soap.getDomesticAirlinesTime("北京", "上海", "2021-10-09","").getAny());
    }

}
