> 汇集了webservice免费接口，使用java完成对接 本代码为接口客户端代码，包括： 1.中国股票 2.电视节目 3.航班 4.ip地址 5.手机号查询 6.QQ在线查询 7.随机字符生成 8.火车时间表 9.天气查询等

### 其他常用接口地址：https://www.cnblogs.com/jpfss/p/8397596.html

### 如遇以下报错：
    [ERROR] undefined element declaration 's:schema'
    http://www.webxml.com.cn/webservices/DomesticAirline.asmx?wsdl??? 15 ??

### 解决方式：
    1、将网址使用ctrl+s保存到本地
    2、将<s:element ref="s:schema" /> <s:any /> 修改为 <s:any minOccurs="2" maxOccurs="2" />
    3、将后缀xml修改为wsdl
    4、导入使用